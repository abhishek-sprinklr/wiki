Rule Engine Overview
===================

<img alt="Rule Engine" src="http://i.imgur.com/nLUaoGp.png?2" width="400" />


# Rule
* A rule is a set of conditions and actions connected together like a flow diagram. <img alt="Rule" src="http://i.imgur.com/ogZdmhc.png" width="400" />
* A rule applies on an *asset* `com.spr.enums.AssetClass`, in above image a message, it follows some path in rule and gets updated based on the condition it satified and action which got applied to this asset.
* A rule **cannot have cycles**. Changes from **one rule can not trigger another rule**.
* RuleContext `com.spr.beans.ruleengine.RuleContext` governs on what assets a rule can be applied and what is the execution level, partner or client.
* Rules are event driven. etc. when a post is scheduled, when an inbound message is grabbed, message is modified, message is added/removed from a queue etc.
* Some rules are also [trigger](#trigger) based. eg. Assign messages in a queue among agents fairly. On every trigger we try to assign messages to agents. If a message is assigned it gets removed from queue. If it fails to assign (due to no available agents or agents already having maximum messages assigned), message stays in the queue and it tries again on next trigger.
* In future, we plan to move all rules to event based model.

# Rule Engine Event
* Rule Engine Event `com.spr.events.event.ruleengine.RuleEngineEvent` is serializable bean that can be dispatched by any component but are processed by Rule Engine.
* It is context aware i.e. aware of userId, clientId, partnerId of the context it got fired in.
* It also store Event Source Context `com.spr.events.event.ruleengine.EventSourceContext` i.e. stores metadata about its source, who fired it, when, which module, stacktrace etc.
* It has a messageType. Corresponding to every message type we need to implement its [MessageTypeHandler](#messageTypeHandler) `com.spr.ruleengine.framework.api.MessageTypeHandler`.
* There are other type of events also like Case Rule Engine Event `com.spr.ruleengine.components.universalcase.CaseRuleEngineEvent`, which are processed by Rule Event. 
* An Event is coupled with a JMSDestination `com.spr.events.enums.JMSDestination`, which enforces in which queue this event should go.
* `messageprocessor/resources/spring/spring-listeners.xml` governs which listener listens to which queue. eg. Case Rule Engine Event Listener `com.spr.messageprocessor.listeners.ruleengine.CaseRuleEngineEventListener`

<a name="messageTypeHandler"></a>
### Message Type Handler

It has two functions.

* **Resolve Message:** Given a message context, validates it and populates required fields. eg. populates universal case for case ids present in message context, populates universal messages from their universal message keys
* **Commit Message:** Given a message context it commits the message i.e. commits updates to an asset. eg. updates a message, case etc 

### Workflow
* The boxes we see in Rule Engine webpage are list of workflows.
<img alt="Workflow" src="http://i.imgur.com/vW9zj7A.png" width="600" />
* A Workflow `com.spr.ruleengine.framework.beans.Workflow` contains a rule. It only stores its ruleId. 
* It can be in enabled or disabled state.
* Workflows are of two type.
	* Standard : Triggers automatically
	* On demand : Triggred by user manually
* Workflows are heavily cached. There can be some delay when workflow is saved and when worflow starts running on messages.
* Rule condition or Rule Actions present in the Workflow are called WorkflowItems `com.spr.ruleengine.framework.beans.WorkflowItem`. 
* When a workflow runs.. it just calls start.run()
* Next Workflow item is run automatically by the currect Workflow item based on condition evaluation or action result.
* A workflow stores Workflow Context `com.spr.ruleengine.framework.api.WorkflowContext`. Which contains execution path. 
* A workflowItem requires message context, workflow context and execution context to run.
 

# Rule Engine
* Rule Engine `com.spr.beans.ruleengine.RuleEngine,com.spr.ruleengine.framework.RuleEngineImpl` is responsible for processing a Rule Engine Event.
* For processing it takes a Rule Engine Event, creates Message Context (com.spr.ruleengine.framework.api.MessageContext).
* A `process()` in Rule Engine just converts an event to its message contexts. 
* Fetches workflow for corresponding messages contexts.
* Runs workflows on message context in order.


# UI
* Rest End Point to create Rule com.spr.ruleengine.ui.space.RulesRestAPI#createOrUpdate
* Whenever a rule is created or updated.. we invalidate [](com.spr.ruleengine.runtime.listener.RuleCacheInvalidateBroadcastHandler)
* Rule is saved as it is in mongo
* Validation - Rule Only
* Validation - Rule + Trigger Combination



# Trigger
<a name="trigger"></a>

* What are types of trigger?
* Why there are these many types of triggers?
* Whats the difference
* Why Unrestricted Trigger requires validation and not others

### TODOs
* Improve this wiki, More elaborate
* Inbound Message Handler
* All rules are event driven but we can also use triggers for rule application
* How to check if a rule is applicable on an asset
* Give lots of examples
* Check caching issue and how we can fix it
* More Indepth tutorial with `classes, images and examples`
* How an workflowElement executing gets its context of the path
* Create a client level and partner level how.. see which got applied.. which not..
* 

### More Info
* For every Asset we have a Conditions.java file where we run our evaluate function


# Summary


--
<center>Author: **Abhishek Kumar** [abhishek.kumar@sprinklr.com](mailto:abhishek.kumar@sprinklr.com)

Reviewer: **Yash Luhadiya** [yluhadiya@sprinklr.com](mailto:yluhadiya@sprinklr.com)
</center>
